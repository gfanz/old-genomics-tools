# Axe Demultiplexer

## Description

Axe demultiplexer is used to demultiplex bar coded sequences. It can process combinatorial bar codes from paired end reads. 

## Source

https://github.com/kdmurray91/axe

Or via the debian package axe-demultiplexer

## Documentation

https://axe-demultiplexer.readthedocs.io/en/latest/

## Paper Reference

Preprint: https://www.biorxiv.org/content/early/2017/07/07/160606

Publication: https://doi.org/10.1093/bioinformatics/bty432

## Notes

Use the mismatch with care as some sets of bar codes / enzyme cut site combinations may not have been designed to have proper hamming distances.
