#!/bin/bash
# This program is to set up a cloud image for GBS Data demultiplexing using combinatorial bar codes.
# It depends on a standard Debian 9 cloud instance.

# Install demultiplexer
sudo apt-get -y install  axe-demultiplexer

# Try to run axe-demux with -V and see if it returns AXE in the string.
# If it does, then output GFANZ_TEST_RESULTS=OK. If it does not, then output
# GFANZ_TEST_RESULTS=ERROR.
axe-demux -V | grep AXE >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
