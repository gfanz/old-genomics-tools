#!/bin/bash
# This program is to set up a cloud image for STACKS 2.2
# SNP Calling pipeline for GBS and RADseq. The image does
# not include any alignment software, it is recommended
# that alignments are made first (including data clean-up
# and filtering) separately with one's favourite tool.

# This repends on a standard Debian 9 cloud instance, but
# should work on Ubuntu 16+ as well (untested).

sudo apt-get -y update

# The following packages are dependencies for STACKS.
sudo apt-get -y install python python-pip samtools vcftools bcftools bamtools make g++

# The following packages are for binary data I/O.
sudo apt-get -y install libpng-dev libbamtools-dev zlib1g-dev

# Get STACKS 2.0:
cd /tmp
wget http://catchenlab.life.illinois.edu/stacks/source/stacks-2.2.tar.gz

# Unpack, configre, and make install.
tar xvf stacks-2.2.tar.gz
cd stacks-2.2/
./configure --prefix=/opt/stacks
make
sudo make install

# TODO: This needs to be fixed as it only sets the PATH for the current session.
# PATH needs to be overwritten on bashrc.
export PATH=$PATH:/opt/stacks:/opt/stacks/bin:/opt/stacks/share

# Check if it has installed properly, run ref_map.pl and check if the
# output is what we expected.
ref_map.pl --version 2>&1 | grep 2.2 >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
