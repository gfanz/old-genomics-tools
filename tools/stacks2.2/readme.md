# STACKS 2

## Description

STACKS 2.2 is a tool for GBS and RADseq SNP calling.

## Source

http://catchenlab.life.illinois.edu/stacks/

## Documentation

http://catchenlab.life.illinois.edu/stacks/manual/

## Paper Reference

https://onlinelibrary.wiley.com/doi/abs/10.1111/mec.12354

## Notes

This script depends on samtools/bamtools/etc. included with Debian 9
and up, and should be compatible with Ubuntu 16+. The most important
thing is that the distro repo should provide samtools version > 1.0, as there
were substantial changes in the API from 0.1.X versions.