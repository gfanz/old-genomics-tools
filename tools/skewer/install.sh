#!/bin/bash 
# This program is to set up a cloud image for the skewer from the github repo..
# It depends on a standard Debian 9 cloud instance.

# install build tools and git
sudo apt-get update
sudo apt-get -y install build-essential git 

# create temporary folders, get the code, compile it

mkdir /tmp/install

sudo chown debian:debian /tmp/install

cd /tmp/install

git clone https://github.com/relipmoc/skewer.git

cd skewer

sudo make

# make the executable directory in opt, move binary there

sudo mkdir /opt/skewer

sudo chown debian:debian /opt/skewer

mv skewer /opt/skewer/

# put skewer into path.

echo '# The following puts the skewer executable in the user path.' >> ~/.bashrc
echo 'export PATH="/opt/skewer:$PATH"' >> ~/.bashrc

# Try skewer --version and see if it is installed.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

/opt/skewer/skewer --version | grep skewer >> /dev/null

if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
else
  echo GFANZ_TEST_RESULTS=ERROR
fi
