#!/usr/bin/perl

# In general, the loops go looking for the FIRST line of the NEXT step after the
# one it's looking for. This is because, when you have multiple libraries, the
# lines it's looking for will be present more than once (sometimes). So just keep
# going till you find the next step and stop right before then!

open (READ, $ARGV[0]);
while (<READ>) {
    print;
    if (/^rename_filter_fastq/) {
        # Step 1! Go through until you find the "head -q -n 40000" line
        while (<READ>) {
            if (/^head -q -n \d+/) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    elsif (/^log Creating mer database for Quorum/) {
        # Step 2! Go through until you find the "log Error correct PE" line
        while (<READ>) {
            if (/^log Error correct PE/) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    elsif (/^quorum_error_correct_reads/) {
        # Step 3! Go thorugh until you find the "if [ -s ESTIMATED_GENOME_SIZE.txt ]" line
        while (<READ>) {
            if (/^if \[ -s ESTIMATED_GENOME_SIZE.txt \]/) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    elsif (/^create_k_unitigs_large_k/) {
        # Step 4! Go through until you find the "log 'Computing super reads from PE" line
        while (<READ>) {
            if (/^log .Computing super reads from PE/) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    elsif (/^create_sr_frg.pl/) {
        # Step 5! Go through until you find the "log 'Celera Assembler'" line
        while (<READ>) {
            if (/^log .Celera Assembler./) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    elsif (/^runCA.+stopAfter.initialStoreBuilding/) {
        # Step 6! Go through until you find the "if [ ! -e CA/overlapFilter.success" line
        # This one encompasses more than one step of the Celera Assembler, unfortunately.
        # Not optimal!
        while (<READ>) {
            if (/^if . . -e CA\/overlapFilter.success/) {
                print "\nexit\n";
                exit;
            }
            else {
                print;
            }
        }
    }
    # TO DO: Look if another step can be added! The difference between the final assembly
    # step and the scaffolder can be huge, but unfortunately it doesn't look like the CA
    # can resume well between steps, so they have to be done in one go. Sad!
}
close READ;
