# MaSuRCA 3.2.8

## Description

MaSuRCA is a genome assembler for large genomes with complex repeat content.
It depends on Illumina short reads, but can also use mate pair data and
long third generation data (PacBio OR Oxford Nanopore, but not both at the
same time!)

## Source

https://github.com/alekseyzimin/masurca

Use relase versions, not development repo:

https://github.com/alekseyzimin/masurca/releases

## Documentation

https://github.com/alekseyzimin/masurca/blob/master/README.md

## Paper Reference

https://academic.oup.com/bioinformatics/article/29/21/2669/195975

## Notes

The install script installs all dependencies including yaggo and swig,
for compatibility purposes. MaSuRCA releases include a version of both,
but any updates through github will break the dependencies, so it's
safer to have them as part of the system install going forward.

This repo includes a couple of extra scripts for running MaSuRCA with
resizable instances in the Catalyst Cloud.

find_step.pl : MaSuRCA reads a configuration file and outputs a bash script
            called "assemble.sh" that will run the assembler. This script
            will look for the first available checkpoint, and cut the
            output script down to that step. This way it is possible to
            run MaSuRCA one step at a time. It HAS to be installed in the
            instance where MaSuRCA is installed, and available in the
            path.
        
launch_masurca.pl: This script will handle running all the checkpoint steps
            with instance sizes defined by a config file (see example_config.txt
            for a very simple example of how this all works). This has to be run
            from a computer with openstack and the right openRC.sh config file,
            and NOT from the instance that will be running MaSuRCA itself.
