#!/usr/bin/perl

my $usage = "$0 [openstack instance name] [instance IP] [SSH key] [remote work directory] [remote config file] [flavour config file]\n";

unless ($#ARGV == 5) {
    die $usage;
}

my %flavour;

open (READ, $ARGV[5]);
LOOP: while (<READ>) {
    if (/^\#/) {
        next LOOP;
    }
    chomp;
    my @x = split;
    $flavour{$x[0]} = $x[1];
}
close READ;

# Begin the 7 step loop for the assembler!
# TODO: Change code so that one can resume from
# the middle of the run without messing around
# with the config file

for my $n (1..7) {
    my $flav = $flavour{"default"};
    if (exists $flavour{$n}) {
        $flav = $flavour{$n};
    }
    print "Resizing $ARGV[0], step $n\n";
    system("nova resize $ARGV[0] $flav");
    sleep 120; # Wait 120s for the instance to reboot
    print "Confirming resize, step $n\n";
    system("nova resize-confirm $ARGV[0]");

    # We're ready! Let's start the loop. Here's what you need to do:

    # 1. enter the work directory
    # 2. Run Masurca with the config file to generate assemble.sh
    # 3. Put assemble.sh through find_step.pl and get the output
    # 4. Run the output by itself in its own ssh line.

    # That's it!

    # TO DO: Gotta check if steps are repeating, because if they
    # do, you won't complete the assembly and you'll needlessly
    # loop through all the steps.

    print "Preparing config file, step $n\n";
    system("ssh -i $ARGV[2] debian\@$ARGV[1] 'cd $ARGV[3] && /opt/masurca/bin/masurca $ARGV[4] && find_step.pl assemble.sh > assemble_step.sh'");
    print "Running assembly, step $n\n";
    system("ssh -i $ARGV[2] debian\@$ARGV[1] 'cd $ARGV[3] && bash assemble_step.sh'");
    print "Done with step $n\n";
}

if (exists $flavour{"minimum"}) {
    my $flav = $flavour{"minimum"};
    system("nova resize $ARGV[0] $flav");
    sleep 60;
    system("nova resize-confirm $ARGV[0]");
}
