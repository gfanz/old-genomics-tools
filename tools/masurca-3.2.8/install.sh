#!/bin/bash

# This program is to set up a cloud image for the
# MaSuRCA genome assembly software. It's fairly large
# and download/compilation can take a while

# This repends on a standard Debian 9 cloud instance, but
# should work on Ubuntu 16+ as well (untested).

sudo apt-get update -y

# The following packages are dependencies for MaSuRCA.
sudo apt-get install -y build-essential libboost-all-dev zlib1g-dev bzip2 libbz2-1.0 libbz2-dev yaggo swig libstatistics-descriptive-perl g++

# Get MaSuRCA 3.2.8
cd /tmp
wget https://github.com/alekseyzimin/masurca/releases/download/3.2.8/MaSuRCA-3.2.8.tar.gz

# Unpack, configure, and install.
tar xvf MaSuRCA-3.2.8.tar.gz
cd MaSuRCA-3.2.8/
./install.sh
rm -rf global-1
rm install.sh PkgConfig.pm sr_config_example.txt
sudo mkdir /opt/masurca
sudo cp -R * /opt/masurca

# TODO: This needs to be fixed because it is only changing the PATH for the
# current session. PATH needs to be overwritten on bashrc file.
export PATH=$PATH:/opt/masurca/bin
echo export PATH=\$PATH:/opt/masurca/bin >> ~/.bashrc
export LD_LIBRARY_PATH=/opt/masurca/lib:/opt/masurca/lib/perl
echo export LD_LIBRARY_PATH=/opt/masurca/lib:/opt/masurca/lib/perl >> ~/.bashrc

# Check if it has installed properly, run ref_map.pl and check if the
# output is what we expected.
masurca -h 2>&1 | grep MaSuRCA >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
