# This is an example configuration file for orchestrating
# multi-step assemblies with MaSuRCA using Openstack
#
# DO NOT use this example! Check the IDs of your cloud's
# flavours:
#
# openstack flavor list
#
# Always include a "default", that flavour will be used
# for all steps unless that step has its own flavour listed
# here.
# Using "minimum" is recommended, the launcher will use
# "minimuuuuuum" to rezise the instance after assembly has
# been complete. That way you don't have to pay for idle
# time on a larger server
default 07585040-f887-4ddb-a0d5-5fac4ff273a7
minimum 99fb31cc-fdad-4636-b12b-b1e23e84fb25
1 0ba691d3-3ff5-4403-843a-c7f8ff61ae4f
3 5b014af3-4ab7-4bb8-8651-f1b55a63feb0
6 bda9bcc5-06ea-428e-8cb2-6eaa2a1cd589
7 68aaca0a-5d27-4a74-840d-1dae21c618b5
