#!/bin/bash

# This program is to set up a cloud image for the BWA aligner version 0.7.15.
# It depends on a standard Debian 9 cloud instance.

# install bwa
sudo apt-get -y install  bwa


# Try which bwa and see if it is installed.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.
which bwa | grep /usr/bin/bwa >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
