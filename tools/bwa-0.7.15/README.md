# bwa

## Description

Burrows-Wheeler Aligner is a tool for aligning sequence reads to a reference.

http://bio-bwa.sourceforge.net/

## Source

https://github.com/lh3/bwa

or the Debian package bwa

## Documentation

http://bio-bwa.sourceforge.net/bwa.shtml

## Paper Reference

See sourceforge page for reference usage.

https://doi.org/10.1093/bioinformatics/btp324

https://doi.org/10.1093/bioinformatics/btp698

## Notes

