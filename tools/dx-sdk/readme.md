# dx-sdk

## Description

DNANexus sdk. This is used to interact with DNANexus from the command line or scripting.

## Source

https://wiki.dnanexus.com/Downloads

## Documentation

Command line quick start: https://wiki.dnanexus.com/Command-Line-Client/Quickstart

Developer portal: https://wiki.dnanexus.com/Developer-Portal

## Paper Reference

Not applicable

## Notes

This installation script uses the python-pip method for install.
