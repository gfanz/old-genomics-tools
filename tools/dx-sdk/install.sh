#!/bin/bash
# This program is to set up a cloud image for interaction with DNA Nexus.
# It depends on a standard Debian 9 cloud instance.

# install pip so that we can use tye pip install process for DNA Nexus tools
sudo apt-get -y install python-pip

# install DX sdk for DNA Nexus
sudo pip install dxpy

# FIXME need to get the next line to automatically source.
# source /etc/profile.d/dnanexus.environment.sh

# Try to run dx and see if it returns DNAnexus in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.
dx | grep DNAnexus >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
