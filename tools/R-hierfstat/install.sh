#!/bin/bash
# This program is to set up a cloud image for R and R packages.
# It depends on a standard Debian 9 cloud instance.

# Install the dependencies necessary to add a new repository over HTTPS and build essential
sudo apt update
sudo apt-get -y install dirmngr apt-transport-https ca-certificates software-properties-common gnupg2 build-essential 

# Enable the CRAN repository and add the CRAN GPG key to your system by running the following commands:
sudo apt-key adv --keyserver keys.gnupg.net --recv-key 'E19F5F87128899B192B1A2C2AD5F960A256A04AF'
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/debian stretch-cran35/'

# update the packages list and install the R package
sudo apt install -y r-base

# Install the R package checkpoint globally
sudo Rscript -e "install.packages('hierfstat', repos='https://cran.stat.auckland.ac.nz')"

# Try listing R packages and check to see if it is installed.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.
Rscript -e "ip = as.data.frame(installed.packages()[,c(1,3:4)]); ip" | grep hierfstat >> /dev/null

if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
else
  echo GFANZ_TEST_RESULTS=ERROR
fi
