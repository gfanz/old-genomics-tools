# owncloudcmd

## Description

This is the command line tool to interact with owncloud / nextcloud / datadrive storage systems.

## Source

The Debian package owncloud-client-cmd 

## Documentation

https://doc.owncloud.org/desktop/2.2/advancedusage.html#owncloud-command-line-client

## Paper Reference

Not Applicable

## Notes
