#!/bin/bash 
# This program is to set up a cloud image for file transfer using owncloud command line
# client. It depends on Debian 9 base image.

# install owcloudcmd
sudo apt-get -y install owncloud-client-cmd

# Try to run owncloudcmd -v and see if it returns version in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

owncloudcmd | grep version >> /dev/null

if [ $? -eq 0 ]; then
  echo "GFANZ_TEST_RESULTS=OK"
else
  echo "GFANZ_TEST_RESULTS=ERROR"
fi
