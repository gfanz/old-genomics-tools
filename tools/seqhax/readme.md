# seqhax

## Description

From the repo text: Small sequence analysis tools that don't deserve their own repos.

## Source

https://github.com/kdmurray91/seqhax

## Documentation

https://github.com/kdmurray91/seqhax

## Paper Reference

Not Applicable

## Notes

It can be used to do the following things (copied from online docs):

    anon       -- Rename sequences by a sequential number
    convert    -- Convert between FASTA and FASTQ formats
    filter     -- Filter reads from a sequence file
    pairs      -- (De)interleave paired end reads
    pecheck    -- Check that paired end reads match properly (also join them)
    preapp     -- Prepend or append string to sequences
    randseq    -- Generate a random sequence file
    stats      -- Basic statistics about sequence files
    trunc      -- Truncate sequences
