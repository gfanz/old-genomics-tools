#!/bin/bash
# This program is to set up a cloud image for the tool GBS-SNP-CROP version 4.0.
# It does not support the paired end options because the software that this GBS-SNP-Crop
# uses for this is not free software. (https://www.h-its.org/en/research/sco/software/#NextGenerationSequencingSequenceAnalysis)
# It depends on a standard Debian 9 cloud instance.

# Install GBS-SNP-CROP dependencies from Debian
sudo apt-get update
sudo apt-get -y install trimmomatic vsearch bwa samtools openjdk-8-jre git

# Install Dependencies that are not documented
sudo apt-get install -y libparallel-forkmanager-perl liblist-moreutils-perl

# Install gbs-snp-crop
cd /opt/
sudo git clone https://github.com/halelab/GBS-SNP-CROP.git

# Change the owner of the GBS-SNP-CROP files to debian
# This may need to be done by user number rather than name
sudo chown -R debian:debian ./GBS-SNP-CROP/

# Make the perl scripts executable.
chmod +x /opt/GBS-SNP-CROP/GBS-SNP-CROP-scripts/v.4.0/*.pl

# Put GBS-SNP-CROP version 4 scripts in the default user path so that the user can run them.
echo '# The following puts the GBS-SNP-CROP scripts in the user path.' >> ~/.bashrc
echo 'export PATH="/opt/GBS-SNP-CROP/GBS-SNP-CROP-scripts/v.4.0:$PATH"' >> ~/.bashrc

# Try to run ipyrad with -h and see if it returns ipyrad in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

#/opt/miniconda2/bin/ipyrad -h | grep ipyrad >> /dev/null

#if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
#else
#  echo GFANZ_TEST_RESULTS=ERROR
#fi

