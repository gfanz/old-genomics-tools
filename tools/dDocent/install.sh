#!/bin/bash
# This program is to set up a cloud image for the tool dDocent version 0.7.28.
# It depends on a standard Debian 9 cloud instance.

# Download and install conda
cd /tmp
sudo mkdir /opt/miniconda2
sudo chown debian:debian /opt/miniconda2
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
bash Miniconda2-latest-Linux-x86_64.sh -f -b -p /opt/miniconda2

# Install dDocent
/opt/miniconda2/bin/conda config --add channels r
/opt/miniconda2/bin/conda config --add channels defaults
/opt/miniconda2/bin/conda config --add channels conda-forge
/opt/miniconda2/bin/conda config --add channels bioconda
/opt/miniconda2/bin/conda  create -y -n ddocent_env ddocent

# Put conda in the default user path so that the user can run things installed
# via conda.
echo '# The following puts the miniconda2 installation in the user path.' >> ~/.bashrc
echo 'export PATH="/opt/miniconda2/bin:$PATH"' >> ~/.bashrc

# export this path so the next steps in the script can use it.

export PATH="/opt/miniconda2/bin:$PATH"

# Activate the dDocent environment

source activate ddocent_env

# Try to run dDocent with -help and see if it returns dDocent in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.


dDocent -help | grep dDocent >> /dev/null

if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
else
  echo GFANZ_TEST_RESULTS=ERROR
fi

