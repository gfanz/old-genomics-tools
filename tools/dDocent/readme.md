# dDocent

## Description

This tool is dDocent. This allows for RAD seq and GBS data analysis. 

## Source

This is the source code repository.

https://github.com/jpuritz/dDocent

## Documentation

http://ddocent.com/

## Paper References

Puritz, J. B., Hollenbeck, C. M., Gold, J. R. dDocent: a RADseq, variant-calling pipeline designed for population genomics of non-model organisms. PeerJ 2:e431 
http://dx.doi.org/10.7717/peerj.431

Puritz, J.B., Matz, M. V., Toonen, R. J., Weber, J. N., Bolnick, D. I., Bird, C. E. Comment: Demystifying the RAD fad. Molecular Ecology 23: 5937–5942. doi: 10.1111/mec.12965 
http://onlinelibrary.wiley.com/doi/10.1111/mec.12965/abstract

## Notes

To start dDocent environment run 

source activate ddocent_env

After that you can use dDocent by running

dDocent
