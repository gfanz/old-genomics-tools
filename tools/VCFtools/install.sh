#!/bin/bash
# This program is to set up a cloud image for the vcftools (0.1.14+dfsg-4).
# It depends on a standard Debian 9 cloud instance.

# install vcftools
sudo apt-get update
sudo apt-get -y install vcftools 

# Try vcftools --version and see if it is installed.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

vcftools --version | grep VCFtools >> /dev/null

if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
else
  echo GFANZ_TEST_RESULTS=ERROR
fi
