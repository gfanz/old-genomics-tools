#!/bin/bash
# This program is to set up a cloud image for the tool ipyRAD version 0.7.28.
# It depends on a standard Debian 9 cloud instance.

# Download and install conda
cd /tmp
wget https://repo.continuum.io/miniconda/Miniconda2-latest-Linux-x86_64.sh
sudo bash Miniconda2-latest-Linux-x86_64.sh -b -p /opt/miniconda2

# Install ipyrad
sudo /opt/miniconda2/bin/conda install -y -c ipyrad ipyrad=0.7.28

# Put conda in the default user path so that the user can run things installed
# via conda.
echo '# The following puts the miniconda2 installation in the user path.' >> ~/.bashrc
echo 'export PATH="/opt/miniconda2/bin:$PATH"' >> ~/.bashrc

# Try to run ipyrad with -h and see if it returns ipyrad in the string.
# If it does, then put GFANZ_TEST_RESULTS=OK into syslog. If it does not, then 
# put GFANZ_TEST_RESULTS=ERROR into syslog.

/opt/miniconda2/bin/ipyrad -h | grep ipyrad >> /dev/null

if [ $? -eq 0 ]; then
  echo GFANZ_TEST_RESULTS=OK
else
  echo GFANZ_TEST_RESULTS=ERROR
fi

