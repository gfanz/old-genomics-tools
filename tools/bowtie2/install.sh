#!/bin/bash

# This program is to set up a cloud image for the bowtie2 aligner version 2.3.0.
# It depends on a standard Debian 9 cloud instance.

# install bowtie2
apt-get -y install bowtie2
