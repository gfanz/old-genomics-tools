# Bowtie 2

## Description

Bowtie 2 is a tool for aligning sequence reads to a reference genome.

## Source

https://sourceforge.net/projects/bowtie-bio/files/bowtie2/

Or debian package bowtie2

## Documentation

http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml

## Paper Reference

https://www.nature.com/articles/nmeth.1923

## Notes

This install script is for the bowtie2 version included in Debian 9.
