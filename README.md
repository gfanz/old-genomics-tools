# GFANZ Genomics Tools

This repository contains a collection of genomics tools and pipelines.

[![pipeline status](https://gitlab.com/gfanz/genomics-tools/badges/master/pipeline.svg)](https://gitlab.com/gfanz/genomics-tools/commits/master)

## Contributing to this repository

The source code is hosted on https://gitlab.com/gfanz/genomics-tools and a copy
is kept in github.com for convenience. Please submit merge requests via Gitlab.

### Collaboration workflow

1. Clone the repository: `git clone git@gitlab.com:gfanz/genomics-tools.git`
1. Change directory to the cloned repository: `cd genomics-tools`
1. Create a new branch: `git checkout -b branch-name`
1. Hack away!
1. Commit when done: `git add . && git commit`
1. Push changes back to Gitlab: `git push --set-upstream origin branch-name`
1. Confirm at https://gitlab.com/gfanz/genomics-tools/pipelines that the
   continuous integration tests passed for the changes you did.
1. Go to https://gitlab.com/gfanz/genomics-tools/merge_requests create a merge
   request.

## Definitions

* ``tool`` is something that performs a specific job, such as "axe-demux" to
  run a demultiplexing step of a given pipeline.
* ``pipeline`` is a sequence of steps performed in a given order, each one
  running its own tool, linearly or in parallel (similar to the definition of a
  workflow).

## Conventions

All tools in this repository follow the conventions listed below:

1. Install scripts must be placed on ``tools/${TOOL_NAME}/install.sh``.
1. If installed from external repositories or source code, binaries must be
   placed on ``/opt`` and added to ``$PATH``.
1. Temporary build files (such as source code packages or tarballs) must be
   stored on ``/tmp``.
1. All research data must be persisted on ``/mnt`` and never on the root volume
   of the compute instance. It is assumed that an additional block storage
   volume will be mounted to `/mnt` and continue to exist after the compute
   instance is deleted.

## Guidelines for working within this repo.

Things to do to make everyone's life easier.

 1. Work with one tool at a time. 
 1. Commit often.
 1. Make commit self contained (i.e. does just one thing or several closely related things)
