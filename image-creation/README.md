# Image Creation Tools

A collection of tools for creating cloud machine and docker images.

## Image creation process

### Tools for image creation

#### Catalyst Cloud

The ``catalystcloud/create-machine-image.sh`` script creates machine
images on the Catalyst Cloud in New Zealand when provided with:

* ``--image``: The operating system image that will be used as a base
* ``--base-config-script``: A cloud-init script that prepares the base operating
  system (e.g.: set timzone, apply security updates, creates users, etc.)
* ``--app-install-script``: A cloud-init script that installs the tools /
  application on top of the configured base operating system.
* ``--key-name``: The SSH key that will be used to generate the image.
* ``--machine-image-name``: The name for the machine image that will be created
  (please note that a timestamp suffix will be automatically appended to it for
  versioning). If not present, then an image will not be created (only tests
  will run).

For example, the command below will build a cloud machine image called `tassel3`
using the `debian-8-x86_64` as its base image and adding the `base-debian.yml`
and `/tools/tassel3/install.sh` as the layers on top of it:
``./create-machine-image.sh -i debian-8-x86_64 -b ../base/base-debian.yml -a ../../tools/tassel3/install.sh -k key_name -m tassel3``

#### Workflow to create images

The workflow the script follows to create cloud machine images is:

1. Launch a compute instance and apply base operating system and configuration
   to it (using a cloud-init config file).
1. Run script that installs the genomics tool(s). Once the set up is complete,
   this install script must run tests to validate it works and output either
   "GFANZ_TEST_RESULTS=OK" or "GFANZ_TEST_RESULTS=ERROR" to the system
   log.
1. Check the console log output from the compute instance to confirm that all
   tests passed. If yes, snapshot the compute instance and create a new image
   from it.
1. Wait until snapshot has been successfuly created (please note this may take
   a few minutes, because instead of using a copy-on-write snapshot the script
   actually copies the complete data set to produce an independent image).
1. Copy the image to all regions of the cloud.
