#!/bin/bash
# Creates a compute instance on the Catalyst Cloud

################################################################################
# Configuration
################################################################################

# Use the Hamilton region of the Catalyst Cloud, where REANNZ connectivity is
# available and data transfer to/from is free to REANNZ members.
#export OS_REGION_NAME="nz-hlz-1"

# The amount of CPU and RAM to allocate
FLAVOR="c1.c1r1"

# Image name
IMAGE_NAME="debian-9-x86_64"

# Name of the public and private networks (public-net and private-net are the
# default)
PUBLIC_NET_NAME="public-net"
PRIVATE_NET_NAME="private-net"

# Name of the temporary compute instance created by this script for producing
# the machine image. The variable "CI_JOB_ID" is set by the Gitlab runners. If
# not present, then a timestamp will be used as prefix.
if ! [ -z "${CI_JOB_ID}" ]; then
  INSTANCE_NAME="gfanz-${CI_JOB_ID}"
else
  INSTANCE_NAME="gfanz-$(date +'%Y-%m-%dT%H:%M:%S')"
fi

# Maximum time in minutes the script will wait for the snapshot and the image
# creation to complete before aborting
MAX_WAIT_TIME_MINUTES=120

################################################################################
# Functions
################################################################################

function help {
  echo "Usage: $0 [-i base_image] -b base_config_script -a install_script [-k key_name] [-m machine_image_name]"
  echo ""
  echo "-i | --image               The operating system image that will be used"
  echo "                           as a base (Default: ${IMAGE_NAME})."
  echo "-b | --base-config-script  A cloud-init script that prepares the base"
  echo "                           operating system image."
  echo "-a | --app-install-script  A cloud-init script that installs the tools"
  echo "                           / application on top of the configured base"
  echo "                           image."
  echo "-k | --key-name            The SSH key that will be used to generate"
  echo "                           the image."
  echo "-m | --machine-image-name  The name for the machine image that will be"
  echo "                           created (please note that a timestamp suffix"
  echo "                           will be appended to it)."
  echo "-t | --test-only           Run tests only and delete compute instance"
  echo "                           (a snapshot and machine image will not be "
  echo "                           created)."
  echo ""
}

function error {
  echo "Error: $*"
  echo ""
  cleanup
  exit 1
}

function warning {
  echo "Warning: $*"
  echo ""
}

# Parse command line arguments
function parse_arguments {
  PARAMS=""
  while (( "$#" )); do
    case "${1}" in
      -i|--image)
        IMAGE_NAME="${2}"
        shift 2
        ;;
      -b|--base-config-script)
        BASE_CONFIG_SCRIPT="${2}"
        shift 2
        ;;
      -a|--app-install-script)
        INSTALL_SCRIPT="${2}"
        shift 2
        ;;
      -k|--key-name)
        KEY_NAME="${2}"
        shift 2
        ;;
      -m|--machine-image-name)
        DATE_SUFFIX=$(date +"%Y-%m-%dT%H:%M:%S")
        MACHINE_IMAGE_NAME="${2}-${DATE_SUFFIX}"
        echo $MACHINE_IMAGE_NAME
        shift 2
        ;;
      -t|--test-only)
        TEST_ONLY="true"
        shift
        ;;
      -h|--help)
        help
        shift
        exit 0
        ;;
      --) # end argument parsing
        shift
        break
        ;;
      -*|--*=) # unsupported option
        echo "Error: Unsupported option ${1}" >&2
        exit 1
        ;;
      *) # preserve positional arguments
        PARAMS="${PARAMS} ${1}"
        shift
        ;;
    esac
  done
  # set positional arguments in their proper place
  eval set -- "${PARAMS}"
}

# Tests connectivity to the Catalyst Cloud using a simple API call
function test_connectivity {
  if ! openstack server list 1>/dev/null 2>&1; then
    error "Could not connect to the Catalyst Cloud."
  fi
}

# Creates a compute instance on the Catalyst Cloud (OpenStack)
# Input:
#   $1 - $USER_DATA_FILE
function create_instance {
  if [ -z "${1}" ]; then
    error "create_instance() requires a USER_DATA_FILE as an argument."
  else
    USER_DATA="${1}"
  fi

  # Find out the ID of the private network in this region
  PRIVATE_NET_ID=$(openstack network list | grep ${PRIVATE_NET_NAME} | awk '{print $2}')

  # Create a compute instance
  if [ -z "${KEY_NAME}" ]; then
    INSTANCE_ID=$(openstack server create --flavor ${FLAVOR} --image ${IMAGE_NAME} --security-group default --nic net-id=${PRIVATE_NET_ID} --user-data ${USER_DATA} ${INSTANCE_NAME} | awk '/\| id/ {print $4}')
  else
    INSTANCE_ID=$(openstack server create --key-name ${KEY_NAME} --flavor ${FLAVOR} --image ${IMAGE_NAME} --security-group default --nic net-id=${PRIVATE_NET_ID} --user-data ${USER_DATA} ${INSTANCE_NAME} | awk '/\| id/ {print $4}')
  fi
  # Find out the ID of the public network (public-net) in this region
  PUBLIC_NET_ID=$(openstack network list | grep ${PUBLIC_NET_NAME} | awk '{print $2}')

  # Allocate a floating IP, so that the compute instance can be made to networks
  # outside the cloud
  FLOATING_IP=$(openstack floating ip create ${PUBLIC_NET_ID} | grep "^| name" | awk '{print $4}')

  # Associate the floating IP to the compute instance
  openstack server add floating ip ${INSTANCE_ID} ${FLOATING_IP}
}

# Wait until a compute instance state is active
# Input:
#   $1 - $INSTANCE_ID
function wait_until_active {
  if [ -z "${1}" ]; then
    error "wait_until_active() requires INSTANCE_ID as an argument."
  else
    ID="${1}"
  fi
  until [[ $(openstack server show ${ID} | awk '/OS-EXT-STS:vm_state/ {print $4}') == "active" ]]; do
    sleep 10
  done
}

# Set an alarm that will trigger when $MAX_WAIT_TIME_MINUTES is reached
function set_alarm {
  trap abort SIGALRM
  PID=$$
  MAX_WAIT_TIME_SECONDS=$((MAX_WAIT_TIME_MINUTES * 60))
  exec sh -c "sleep ${MAX_WAIT_TIME_SECONDS} && kill -ALRM $PID" &
  PID_ALARM=$!
}

# Abort the execution of the $MAX_WAIT_TIME_MINUTES alarm is triggered
function abort {
  error "Reached the maximum wait time. Aborting execution"
}

# Wait for test results on the console output
# Input:
#   $1 - $INSTANCE_ID
function wait_for_test_results {
  if [ -z "${1}" ]; then
    error "wait_for_test_results() requires INSTANCE_ID as an argument."
  else
    ID="${1}"
  fi
  until openstack console log show ${ID} | grep -q "GFANZ_TEST_RESULTS"; do
    sleep 30
  done
  TEST_RESULTS=$(openstack console log show ${ID} | grep GFANZ_TEST_RESULTS | cut -d"=" -f2)
  if [[ "${TEST_RESULTS}" == "OK" ]]; then
    return 0
  else
    return 1
  fi
}

# Create a cloud-config file using the MIME format
function create_user_data {
  USER_DATA_FILE=$(mktemp)
  DATE=$(date +"%a %b %d %T %Y")
  echo "Content-Type: multipart/mixed; boundary=\"===============1123581321345589144==\"" >> ${USER_DATA_FILE}
  echo "MIME-Version: 1.0" >> ${USER_DATA_FILE}
  echo "" >> ${USER_DATA_FILE}
  for FILE in "$@"; do
    if file ${FILE} | grep -q "shell script"; then
      MIME_TYPE="text/x-shellscript"
    else
      MIME_TYPE="text/cloud-config"
    fi
    echo "--===============1123581321345589144==" >> ${USER_DATA_FILE}
    echo "MIME-Version: 1.0" >> ${USER_DATA_FILE}
    echo "Content-Type: ${MIME_TYPE}; charset=\"us-ascii\"" >> ${USER_DATA_FILE}
    echo "Content-Transfer-Encoding: 7bit" >> ${USER_DATA_FILE}
    echo "Content-Disposition: attachment; filename=\"$(basename ${FILE})\"" >> ${USER_DATA_FILE}
    echo "" >> ${USER_DATA_FILE}
    cat ${FILE} >> ${USER_DATA_FILE}
    echo "" >> ${USER_DATA_FILE}
  done
  echo "--===============1123581321345589144==--" >> ${USER_DATA_FILE}
}

# Creates a snapshot of a compute instance
# Input:
#   $1 - $COMPUTE_INSTANCE_ID
function create_snapshot {
  if [ -z "${1}" ]; then
    error "create_snapshot() requires a COMPUTE_INSTANCE_ID as an argument."
  else
    ID="${1}"
  fi

  # Create the snapshot and wait for it to complete
  openstack server image create --wait --name ${MACHINE_IMAGE_NAME} ${ID}
  return ${?}
}

# Cleans up temporary resources created to generate the machine image
function cleanup {
  # If a compute instance was created, delete it
  if [ -n "${INSTANCE_ID}" ]; then
    if ! openstack server delete ${INSTANCE_ID}; then
      warning "Cloud not delete instance ${INSTANCE_ID}"
    fi
  fi
  # If a floating IP was allocated, delete it
  if [ -n "${FLOATING_IP}" ]; then
    if ! openstack floating ip delete ${FLOATING_IP}; then
      warning "Could not delete floating IP ${FLOATING_IP}"
    fi
  fi
  # Cancel the alarm if it was set
  if [ -n "${PID_ALARM}" ]; then
    kill ${PID_ALARM}
  fi
}

################################################################################
# Main ()
################################################################################

# If no arguments were passed, show help and exit
if [[ $# -eq 0 ]]; then
  help
  exit 0
fi

# Parse command line arguments
parse_arguments "$@"
if [ -z "${BASE_CONFIG_SCRIPT}" ]; then
  error "Base config script (-b) argument is required."
fi
if [ -z "${INSTALL_SCRIPT}" ]; then
  error "Install script (-i) argument is required."
fi

# Test connectivity to the Catalyst Cloud
echo "Testing connectivity to the Catalyst Cloud"
test_connectivity
echo "OK"
echo ""

# Create a cloud-init script using the multi-part MIME format that combines both
# the base config script (the script that prepares the operating system image)
# with the application install script
echo "Creating the user-data script that combines the os_config and app_install scripts"
create_user_data ${BASE_CONFIG_SCRIPT} ${INSTALL_SCRIPT}
echo "OK"
echo ""

# Launch a compute instance using the newly multi-part user data script as its
# user data
echo "Launching a compute instance and applying the user-data script"
create_instance ${USER_DATA_FILE}
echo "OK"
echo ""

# The following two actions in the workflow (waiting until the application
# script finished and creating the snapshot) could take too long to complete if
# there is a bug in the code or incident with the cloud, so we set an alarm
# that waits for $MAX_WAIT_TIME_MINUTES and aborts script if the alarm is
# triggered
set_alarm

# Wait until the compute instance has booted and the scripts finished
echo "Waiting for application install and automated tests to complete"
wait_until_active ${INSTANCE_ID}
if ! wait_for_test_results ${INSTANCE_ID}; then
  error "Tests have failed. Aborting execution."
fi
echo "OK"
echo ""

# These last two steps will only run if a machine image name has been defined
if [ -z ${TEST_ONLY} ]; then
  # Create a snapshot of the compute instance and wait until it has finished
  echo "Snapshoting the instance and creating a machine image from it"
  if ! create_snapshot ${INSTANCE_ID}; then
    error "Failed to create snapshot for instance ${INSTANCE_ID}."
  else
    echo "Image ${MACHINE_IMAGE_NAME} successfully created!"
  fi
  echo "OK"
  echo ""

  # Copy the machine image to all regions of the Catalyst Cloud
  echo "Copying the machine image to all region of the Catalyst Cloud"
  # TODO: copy image function
  echo "OK"
  echo ""

fi

# Clean up temporary compute intance and floating IP
echo "Cleaning up the temporary compute instance and floating IP allocated"
cleanup
echo "OK"
echo ""

exit 0
