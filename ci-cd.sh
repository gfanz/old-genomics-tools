#!/bin/bash

###########
# Functions
###########

# Run tests for a specific tool in the repository
# Input:
#   $1 - $TOOL
function run_tests {
  TOOL=${1}
  TOOL_DIRECTORY="tools/${TOOL}"
  echo "Testing ${TOOL_DIRECTORY}"
  INSTALL_SCRIPT="${TOOL_DIRECTORY}/install.sh"
  image-creation/default/create-image.sh --test-only --image debian-9-x86_64 --base-config-script image-creation/base/base-debian.yml --app-install-script ${INSTALL_SCRIPT}
  RETURN_CODE=$?
  if [[ ${RETURN_CODE} -ne 0 ]]; then
    exit ${RETURN_CODE}
  fi
}

# Build image for a specific tool in the repository
# Input:
#   $1 - $TOOL
function build_image {
  TOOL="${1}"
  TOOL_DIRECTORY="tools/${TOOL}"
  INSTALL_SCRIPT="${TOOL_DIRECTORY}/install.sh"
  MACHINE_IMAGE_NAME="gfanz-${TOOL}"
  echo "Building ${TOOL_DIRECTORY}"
  image-creation/default/create-image.sh --image debian-9-x86_64 --base-config-script image-creation/base/base-debian.yml --app-install-script ${INSTALL_SCRIPT} --machine-image-name ${MACHINE_IMAGE_NAME}
  RETURN_CODE=$?
  if [[ ${RETURN_CODE} -ne 0 ]]; then
    exit ${RETURN_CODE}
  fi
}

#########
# Main ()
#########

# Check what tools have been modified compared to the master branch
TOOLS_MODIFIED=$(git diff --raw HEAD^ HEAD | cut -d$'\t' -f2 | grep "^tools/" | cut -d'/' -f2 | sort | uniq)

# If nothing changed, exit the job immediately, otherwise install the
# dependencies to run job on the default cloud provider
if [[ ${TOOLS_MODIFIED} == "" ]]; then
  echo "No tools modified. Nothing to do."
  exit 0
fi

# Run tests or build for tools that had their code changed since the last commit
for TOOL in ${TOOLS_MODIFIED}; do
 case "${CI_JOB_STAGE}" in
    "test")
      run_tests "${TOOL}"
      ;;
    "build")
      build_image "${TOOL}"
      ;;
    *)
      echo "The ${CI_JOB_STAGE} CI_JOB_STAGE is not valid. Aborting."
      exit 1
      ;;
  esac
done

exit 0
